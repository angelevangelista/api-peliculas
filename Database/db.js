var sql = require("mssql");

var config = {
    user: 'angellizander',
    password: '123456',
    driver: 'msnodesql',
    database: 'Proyecto',
    server: 'localhost',
    connectionString: "Driver={SQL Server Native Client 11.0};Server=#{server}\\SQLEXPRESS;Database=#{database};Uid=#{user};Pwd=#{password};"
};

const poolPromise = new sql.ConnectionPool(config)
  .connect()
  .then(pool => {
    console.log('Conexion MSSQL iniciada!')
    return pool
  })
  .catch(err => console.log('Conexion incorrecta a DB: ', err))

module.exports = {
  sql, poolPromise
}
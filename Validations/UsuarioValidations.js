const Joi = require('joi'); 

const Usuario = Joi.object().keys({ 
  User: Joi.string()
  .alphanum().
  min(3)
  .max(15)
  .required(),
  Password: Joi.string().min(6).max(30),
  Names: Joi.string().min(3).max(60),
  Surnames: Joi.string().min(4).max(100),
  Id: Joi.string().length(11)
}); 

module.exports = {Usuario, Joi}
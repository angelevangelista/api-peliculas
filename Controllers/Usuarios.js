const { sql,poolPromise } = require('../Database/db')
const fs = require('fs');
const {Usuario, Joi}  = require('../Validations/UsuarioValidations');
var rawdata = fs.readFileSync('./Queries/UsuariosQ.json');
var queries = JSON.parse(rawdata);

class MainController {

  
    async getUsers(req , res){
      try {
        const pool = await poolPromise
          const result = await pool.request()
          .query(queries.GetUsers)
          res.json(result.recordset)
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }

    async getUser(req , res){
      try {
        const pool = await poolPromise
          const result = await pool.request()
          .input('Id',sql.VarChar, req.body.Id)
          .query(queries.GetUser)
          res. json(result.recordset)
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }



    async addUser(req , res){

      const validation = Joi.validate(req.body,Usuario); 


      try {
        if(validation.error == null) {
          const pool = await poolPromise
          const result = await pool.request()
          .input('User',sql.VarChar , req.body.User)
          .input('Password',sql.VarChar,req.body.Password)
          .input('Names',sql.VarChar , req.body.Names)
          .input('Surnames',sql.VarChar , req.body.Surnames)
          .input('Id',sql.VarChar,req.body.Id)
          .query(queries.AddUser)
          res.json(result)
        } else {
          res.send(validation.error.message)
        }
      } catch (error) {
        res.status(500)
        res.send(error.message)
    }
    }


    async updateUser(req , res){
      try {
        if(req.body.newPassword != null && req.body.userName != null) {
        const pool = await poolPromise
          const result = await pool.request()
          .input('newPassword',sql.VarChar , req.body.newPassword)
          .input('userName',sql.VarChar,req.body.userName)
          .query(queries.deleteUser)
          res.json(result)
        } else {
          res.send('All fields are required!')
        }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }


    async deleteUser(req , res){
      try {
        if(req.body.Name != null ) {
          const pool = await poolPromise
            const result = await pool.request()
            .input('userName',sql.VarChar,req.body.Name)
            .query(queries.DeleteUser)
            res.json(result)
          } else {
            res.send('Please fill all the details!')
          }
      } catch (error) {
        res.status(500)
        res.send(error.message)
      }
    }
}

const Usuarios = new MainController()
module.exports = Usuarios;
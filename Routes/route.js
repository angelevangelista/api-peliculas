const express =  require('express');
const Usuarios = require('../Controllers/Usuarios')

const router = express.Router();

//#region Controller Usuarios
router.get('/api/getUsers', Usuarios.getUsers);
router.get('/api/getUser', Usuarios.getUser);
router.post('/api/addUser' , Usuarios.addUser);
router.put('/api/updateUser',Usuarios.updateUser);
router.delete('/api/deleteUser' , Usuarios.deleteUser);
//#endregion



module.exports = router;
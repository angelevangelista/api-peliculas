const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const fs = require('fs')
const path = require('path')
const morgan = require('morgan')
const router = require('./Routes/route')
const app = express()


app.use(cors())

// codificador a application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// convertidor application/json
app.use(bodyParser.json())

app.use(morgan('dev'))

// creador de log
var accessLogStream = fs.createWriteStream(path.join(__dirname, '/logs/access.log'), { flags: 'a' })
 
// setup the logger
app.use(morgan('combined', { stream: accessLogStream }))

app.use(router)

const port = 3000

app.listen(process.env.PORT || port , (err) => {
  if(err)
console.log('Error al iniciar server!')
else
console.log('Server started running on : ' + port)
})